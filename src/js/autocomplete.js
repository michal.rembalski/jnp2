import React, {useEffect, useState} from "react"

const AutoComplete = () => {
    const [search, setSearch] = useState("")
    const [options, setOptions] = useState([])
    const [show, setShow] = useState(false)
    
    useEffect(() => {
        console.log(("XD"))
        setOptions([{"name": "H"}])
    }, [])

    return (
        <div>
            <input 
                id="search"
                placeholder="Type, bro"
            >
            </input>
            {
                show && (
                    <div id="options">
                        {
                            options.map(el => <div>{el.name}</div>)
                        }
                    </div>
                )
            }
        </div>
    )
}

export default AutoComplete